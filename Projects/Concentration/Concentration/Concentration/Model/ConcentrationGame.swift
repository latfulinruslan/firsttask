//
//  Concentration.swift
//  Concentration
//
//  Created by Ruslan Latfulin on 4/22/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

class ConcentrationGame {

    private (set) var cards = [ConcentrationCard]()
    private var indexOfOneAndOnlyFaceUpCard: Int?
    var flipCount = 0
    private (set) var score = 0

    init(numberOfPairsOfCards: Int) {
        for _ in 1...numberOfPairsOfCards {
            let card = ConcentrationCard()
            cards += [card, card]
        }
        cards.shuffle()
    }

    func chooseCard(at index: Int) {
        guard !cards[index].isMatched else { return }

        guard let matchIndex = indexOfOneAndOnlyFaceUpCard else {
            leaveCardFaceUp(at: index)
            return
        }

        if matchIndex != index {
            checkCards(matchIndex, index)
            cards[matchIndex].isWatched = true
            cards[index].isWatched = true
            matchCards(matchIndex, index)
            cards[index].isFaceUp = true
            indexOfOneAndOnlyFaceUpCard = nil
        }
    }

    private func matchCards(_ firstCardIndex: Int, _ secondCardIndex: Int) {
        guard cards[firstCardIndex].identifier == cards[secondCardIndex].identifier else { return }
        cards[firstCardIndex].isMatched = true
        cards[secondCardIndex].isMatched = true
    }

    private func checkCards(_ firstCardIndex: Int, _ secondCardIndex: Int) {
        if cards[firstCardIndex].identifier == cards[secondCardIndex].identifier {
            score += 2
        } else {
            if cards[firstCardIndex].isWatched { score -= 1}
            if cards[secondCardIndex].isWatched {score -= 1}
        }
    }

    private func leaveCardFaceUp(at index: Int) {
        for flipDownIndex in cards.indices {
            cards[flipDownIndex].isFaceUp = false
        }
        cards[index].isFaceUp = true
        indexOfOneAndOnlyFaceUpCard = index
    }
}
