//
//  Card.swift
//  Concentration
//
//  Created by Ruslan Latfulin on 4/22/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

struct ConcentrationCard {
    var isFaceUp = false
    var isMatched = false
    var isWatched = false
    var identifier: Int

    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }

    private static var identifierFactory = 0

    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
}
