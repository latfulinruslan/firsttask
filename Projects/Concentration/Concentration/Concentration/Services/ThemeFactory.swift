//
//  ThemeFactory.swift
//  Concentration
//
//  Created by Ruslan Latfulin on 4/23/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class ThemeFactory {
    static let instance = ThemeFactory()

    private var emoji: [[String]] = []

    init () {
        emoji.append(["🐶", "🐱", "🦊", "🐻", "🐷", "🐥", "🐴", "🦁"])
        emoji.append(["🍏", "🍎", "🍅", "🌽", "🥥", "🍕", "🥮", "🥝"])
        emoji.append(["⚽️", "🏀", "🏈", "⚾️", "🥎", "🏐", "🎱", "🥏"])
        emoji.append(["😀", "😋", "😠", "🤩", "🤪", "🤣", "😂", "🤓"])
        emoji.append(["💸", "💵", "💴", "💶", "💷", "💰", "💳", "💎"])
        emoji.append(["🔺", "🔻", "🔸", "🔴", "🔷", "♦️", "♠️", "♣️"])
        emoji.append(["✈️", "🚜", "🚒", "🚢", "🚘", "🚎", "🚗", "🚀"])
    }

    func getTheme(for index: Int) -> [String] {
        return emoji[index]
    }
}
