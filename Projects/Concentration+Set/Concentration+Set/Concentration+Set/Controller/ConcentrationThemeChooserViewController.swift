//
//  ConcentrationThemeChooserViewController.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/12/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {

    @IBOutlet var themeButtons: [UIButton]!
    private var themeCreator = ThemeFactory.instance
    private var lastSeguedtToConcentrationViewController: ConcentrationViewController?
    private var splitDetailViewController: ConcentrationViewController? {
        return splitViewController?.viewControllers.last as? ConcentrationViewController
    }

    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        if let cvc = secondaryViewController as? ConcentrationViewController {
            guard cvc.theme == nil else { return false }
        }
        return true
    }
    
    @IBAction func pushThemeButtonAction(_ sender: UIButton) {
        guard let themeIndex = themeButtons.firstIndex(of: sender) else { return }
        let choosenTheme = themeCreator.getTheme(for: themeIndex)
        if let cvc = splitDetailViewController {
            cvc.theme = choosenTheme
        } else if let cvc = lastSeguedtToConcentrationViewController {
            cvc.theme = choosenTheme
            navigationController?.pushViewController(cvc, animated: true)
        } else {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let concentrationViewController = storyboard.instantiateViewController(withIdentifier: "ConcentrationSID") as! ConcentrationViewController
            concentrationViewController.theme = choosenTheme
            lastSeguedtToConcentrationViewController = concentrationViewController
            self.navigationController?.pushViewController(concentrationViewController, animated: true)
        }
    }
}
