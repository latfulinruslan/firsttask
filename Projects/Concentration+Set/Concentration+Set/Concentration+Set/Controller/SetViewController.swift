//
//  SetViewController.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class SetViewController: UIViewController {
    
    private var game = SetGame()
    
    @IBOutlet weak var gameView: SetGameView! {
        didSet {
            let swipe = UISwipeGestureRecognizer(target: self, action: #selector(addThreeCards))
            swipe.direction = .down
            gameView.addGestureRecognizer(swipe)

            let rotation = UIRotationGestureRecognizer(target: self, action: #selector(reshuffle(sender:)))
            gameView.addGestureRecognizer(rotation)
        }
    }
    
    lazy var animator = UIDynamicAnimator(referenceView: view)
    lazy var cardBehavior = CardBehavior(in: animator)
    
    @IBOutlet weak var controlPanel: UIStackView!
    @IBOutlet private weak var addThreeCardsButton: UIButton!
    @IBOutlet weak var dealDeckImage: UIImageView!
    @IBOutlet weak var deckPanel: UIStackView!
    
    private var dealDeck: UIImageView {
        return deckPanel.arrangedSubviews.first as! UIImageView
    }
    private var discardPile: UIImageView {
        return deckPanel.arrangedSubviews.last as! UIImageView
    }
    private var matchedCards: [SetCardView] {
        return gameView.cards.filter { $0.isMatched }
    }
    private var dealCardViews: [SetCardView] {
        return gameView.cards.filter { $0.alpha == 0}
    }
    private var tmpCards = [SetCardView]()
    private var tmpStartPoint: CGPoint {
        return deckPanel.convert(dealDeck.center, to: view)
    }
    private var startPoint: CGPoint {
        return view.convert(tmpStartPoint, to: gameView)
    }
    private var tmpEndPoint: CGPoint {
        return deckPanel.convert(discardPile.center, to: view)
    }
    private var endPoint: CGPoint {
        return view.convert(tmpEndPoint, to: gameView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newGame()
    }
    
    @IBAction private func pushAddThreeCardAction(_ sender: Any) {
        addThreeCards()
    }
    
    @objc private func addThreeCards() {
        guard game.fullDeck.count > 0 else { return }
        game.addCards(numberOfCards: Constants.addMoreCards)
        
        updateViewFromModel()
        if game.fullDeck.isEmpty {
            addThreeCardsButton.isEnabled = false
        }
    }
    
    @objc private func reshuffle(sender: UIRotationGestureRecognizer) {
        guard sender.state == .ended else { return }
        game.reshuffle()
        updateViewFromModel()
    }
    
    @IBAction private func pushNewGameAction(_ sender: Any) {
        newGame()
    }
    
    private func dealAnimation() {
        var currentCard: Int = 0
        let timeInterval = 0.1 * Double(gameView.gridRows)
        
        Timer.scheduledTimer(withTimeInterval: timeInterval,
                             repeats: false,
                             block: { timer in
                                self.dealCardViews.forEach { cardView in
                                    cardView.startAnimation(from: self.startPoint, delay: TimeInterval(currentCard) * 0.2)
                                    currentCard += 1
                                }
                             })
    }
    
    private func awayAnimation() {
        let flyCount =
            matchedCards.filter{($0.alpha < 1 && $0.alpha > 0)}.count
        
        if  game.isSet(cards: game.selectedDeck), flyCount == 0 {
            tmpCards.forEach { cardView in
                cardView.removeFromSuperview()
            }
            
            tmpCards = []
            
            matchedCards.forEach { cardView in
                cardView.alpha = 0.1
                if let newCardView = cardView.copy() as? SetCardView {
                    tmpCards += [newCardView]
                }
            }
            
            tmpCards.forEach { cardView in
                gameView.addSubview(cardView)
                cardBehavior.addItem(cardView)
            }
            flyTimer(tmpCards: tmpCards)
        }
    }
    
    private func flyTimer(tmpCards: [SetCardView]) {
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { timer in
            var j = 1
            for temporaryCard in self.tmpCards {
                self.cardBehavior.removeItem(temporaryCard)
                temporaryCard.animateFly(to: self.endPoint,
                                         delay: TimeInterval(j) * 0.25)
                j += 1
            }
        }
    }
    
    private func newGame() {
        addThreeCardsButton.isEnabled = true
        game = SetGame()
        gameView.newGame()
        updateViewFromModel()
    }
    
    @IBAction private func pushFindSetAction(_ sender: Any) {
        guard game.actualDeck.count > 0 else { return }
        guard game.findSet() != nil else { return }
        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        var newCardViews = [SetCardView]()
        if gameView.cards.count - game.actualDeck.count > 0 {
            gameView.removeCards(matchedCards)
        }
        
        let cardsCount = gameView.cards.count
        for index in game.actualDeck.indices {
            let card = game.actualDeck[index]
            if index > (cardsCount - 1) {
                let newCard = SetCardView()
                updateCardView(newCard, for: card)
                newCard.alpha = 0
                addTapGestureRecognizer(for: newCard)
                newCardViews += [newCard]
            } else {
                let newCard = gameView.cards[index]
                if newCard.alpha < 1 && newCard.alpha > 0 && !(game.isSet(cards: game.selectedDeck)) {
                     newCard.alpha = 0
                }
                updateCardView(newCard, for: card)
            }
        }
        
        gameView.addCards(newCardViews)
        
        awayAnimation()
        
        dealAnimation()
    }
    
    private func updateCardView(_ cardView: SetCardView, for card: SetCard) {
        cardView.symbol = card.symbol
        cardView.color = card.color
        cardView.count = card.number
        cardView.fill = card.filling
        cardView.isSelected = game.selectedDeck.contains(card)
        cardView.isMatched =  game.matchedCards.contains(card)
    }
    
    private func addTapGestureRecognizer(for cardView: SetCardView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapCard(recognizer:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        cardView.addGestureRecognizer(tap)
    }
    
    @objc private func tapCard(recognizer: UITapGestureRecognizer) {
        guard recognizer.state == .ended else { return }
        guard let cardView = recognizer.view as? SetCardView else { return }
        guard let cardIndex = gameView.cards.firstIndex(of: cardView) else { return }
        let card = game.actualDeck[cardIndex] 
        game.chooseCard(card: card)
        updateViewFromModel()
    }
}
