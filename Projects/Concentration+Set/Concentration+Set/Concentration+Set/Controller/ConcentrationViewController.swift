//
//  ViewController.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/12/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class ConcentrationViewController: UIViewController {

    private lazy var game = ConcentrationGame(numberOfPairsOfCards: numberOfPairsOfCards)
    
    private var emojiChoices: [String] = []
    private var emoji = [Int: String]()
    private var buttonColor = UIColor()
    private var numberOfPairsOfCards: Int {
        return (cardButtons.count + 1) / 2
    }
    var theme: [String]? {
        didSet {
            emojiChoices = theme ?? []
            emoji = [:]
            updateViewFromModel()
        }
    }
    @IBOutlet private weak var flipCountLabel: UILabel!
    @IBOutlet private var cardButtons: [UIButton]!
    @IBOutlet private weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewGame()
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        game.flipCount += 1
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    @IBAction func touchNewGameAction() {
        startNewGame()
    }
    
    private func startNewGame() {
        game = ConcentrationGame(numberOfPairsOfCards: numberOfPairsOfCards)
//        var newTheme = ThemeFactory.instance.getTheme()
//
//        guard
//            let buttonColor = newTheme.removeLast() as? UIColor,
//            let backgroundColor = newTheme.removeLast() as? UIColor,
//            let emojiChoices = newTheme as? [String] else { return }
//        self.buttonColor = buttonColor
//        self.view.backgroundColor = backgroundColor
//        self.emojiChoices = emojiChoices
        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        guard cardButtons != nil else { return }
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 0.3543831976, green: 1, blue: 0.9960503558, alpha: 0) : #colorLiteral(red: 0.3709202792, green: 0.6525018202, blue: 1, alpha: 1)
            }
        }
        flipCountLabel.text = "Flips: \(game.flipCount)"
        scoreLabel.text = "Score: \(game.score)"
    }
    
    private func emoji(for card: ConcentrationCard) -> String {
        if emoji[card.identifier] == nil, emojiChoices.count > 0 {
            let randomIndex = Int.random(in: 0..<emojiChoices.count)
            emoji[card.identifier] = emojiChoices.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }


}

