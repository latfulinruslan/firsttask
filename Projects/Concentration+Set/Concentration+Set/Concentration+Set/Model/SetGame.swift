//
//  SetGame.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

class SetGame {
    
    private (set) var fullDeck = [SetCard]()
    private (set) var actualDeck = [SetCard]()
    private (set) var selectedDeck = [SetCard]()
    private (set) var matchedCards = [SetCard]()
    
    init() {
        fullDeck.removeAll()
        actualDeck.removeAll()
        selectedDeck.removeAll()
        matchedCards.removeAll()
        
        for number in CardNumber.allCases {
            for symbol in CardSymbol.allCases {
                for color in CardColor.allCases {
                    for filling in CardFilling.allCases {
                        let card = SetCard(number: number, symbol: symbol, color: color, filling: filling)
                        fullDeck.append(card)
                    }
                }
            }
        }
        fullDeck.shuffle()
        addCards(numberOfCards: Constants.starterKit)
    }
    
    func addCards(numberOfCards: Int) {
        for _ in 0..<numberOfCards {
            let selectedCard = fullDeck.remove(at: fullDeck.count.random())
            actualDeck.append(selectedCard)
        }
    }
    
    func chooseCard(card: SetCard) {
        if selectedDeck.count == 3 && isSet(cards: selectedDeck) {
            selectedDeck.forEach {currCard in
                guard let selectedCardIndex = actualDeck.firstIndex(of: currCard) else { return }
                actualDeck.remove(at: selectedCardIndex)
                if fullDeck.count > 0 {
                    let selectedCard = fullDeck.remove(at: fullDeck.count.random())
                    actualDeck.insert(selectedCard, at: selectedCardIndex)
                }
            }
            selectedDeck.removeAll()
            //matchedCards.removeAll()
            return
        } else if selectedDeck.count == 3 && !isSet(cards: selectedDeck) {
            selectedDeck.removeAll()
            //matchedCards.removeAll()
            return
        }
        
        guard let cardToSelect = selectedDeck.firstIndex(of: card) else {
            selectedDeck.append(card)
            return
        }
        selectedDeck.remove(at: cardToSelect)
    }
    
    func isSet(cards: [SetCard]) -> Bool {
        guard cards.count == 3 else { return false }
        let sum = [
            cards.reduce(0, { $0 + $1.number.rawValue }),
            cards.reduce(0, { $0 + $1.color.rawValue }),
            cards.reduce(0, { $0 + $1.symbol.rawValue }),
            cards.reduce(0, { $0 + $1.filling.rawValue })
        ]
        return sum.reduce(true, { $0 && ($1 % 3 == 0) })
    }
    
    func cardIsSelected(card: SetCard) -> Bool {
        return selectedDeck.firstIndex(of: card) != nil
    }
    
    func reshuffle() {
        actualDeck.shuffle()
    }
    
    func findSet() -> [SetCard]? {
        var checkForSet = [SetCard]()
        selectedDeck.removeAll()
        matchedCards.removeAll()
        
        for firstCardIndex in 0..<actualDeck.count {
            for secondCardIndex in (firstCardIndex + 1)..<actualDeck.count {
                for thirdCardIndex in (secondCardIndex + 1)..<actualDeck.count {
                    checkForSet.append(actualDeck[firstCardIndex])
                    checkForSet.append(actualDeck[secondCardIndex])
                    checkForSet.append(actualDeck[thirdCardIndex])
                    if isSet(cards: checkForSet) {
                        checkForSet.forEach { card in
                            matchedCards.append(card)
                        }
                    return checkForSet
                    } else {
                        checkForSet.removeAll()
                    }
                }
            }
        }
        return nil
    }
}
