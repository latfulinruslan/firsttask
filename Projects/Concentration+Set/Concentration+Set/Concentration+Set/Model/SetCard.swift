//
//  SetCard.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

struct SetCard: Hashable {
    let number: CardNumber
    let symbol: CardSymbol
    let color: CardColor
    let filling: CardFilling
    
    static func == (lhs: SetCard, rhs: SetCard) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

enum CardNumber: Int, CaseIterable {
    case one = 1, two, three
}

enum CardSymbol: Int, CaseIterable {
    case squiggle = 1, oval, diamond
}

enum CardColor: Int, CaseIterable {
    case red = 1, green, purple
}

enum CardFilling: Int, CaseIterable {
    case empty = 1, stripes, solid
}
