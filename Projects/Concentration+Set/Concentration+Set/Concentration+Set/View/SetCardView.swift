//
//  SetCardView.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class SetCardView: UIView {
    
    var symbol: CardSymbol = .oval {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    var fill: CardFilling = .empty {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    var color: CardColor = .green {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    var count: CardNumber = .one {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    var isSelected: Bool = false {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    var isMatched: Bool = false {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    var isFaceUp: Bool = true {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    private let interStripeSpace: CGFloat = 5.0
    private let borderWidth: CGFloat = 5.0
    
    override func draw(_ rect: CGRect) {
        let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        UIColor.white.setFill()
        roundedRect.fill()
        if isFaceUp {
            drawPips()
        } else {
            guard let cardBack = UIImage(named: "cardback") else { return }
            cardBack.draw(in: bounds)
        }
    }
    
    private func drawPips() {
        setColor()
        switch count {
        case .one:
            let origin = CGPoint(x: faceFrame.minX, y: faceFrame.midY - pipHeight/2)
            let size = CGSize(width: faceFrame.width, height: pipHeight)
            let firstRect = CGRect(origin: origin, size: size)
            drawSymbols(rect: firstRect)
        case .two:
            let origin = CGPoint(x: faceFrame.minX, y: faceFrame.midY - interPipHeight/2 - pipHeight)
            let size = CGSize(width: faceFrame.width, height: pipHeight)
            let firstRect = CGRect(origin: origin, size: size)
            drawSymbols(rect: firstRect)
            let secondRect = firstRect.offsetBy(dx: 0, dy: pipHeight + interPipHeight)
            drawSymbols(rect: secondRect)
        case .three:
            let origin = CGPoint (x: faceFrame.minX, y: faceFrame.minY)
            let size = CGSize(width: faceFrame.width, height: pipHeight)
            let firstRect = CGRect(origin: origin, size: size)
            drawSymbols(rect: firstRect)
            let secondRect = firstRect.offsetBy(dx: 0, dy: pipHeight + interPipHeight)
            drawSymbols(rect: secondRect)
            let thirdRect = secondRect.offsetBy(dx: 0, dy: pipHeight + interPipHeight)
            drawSymbols(rect: thirdRect)
        }
    }
    
    private func setColor() {
        switch color {
        case .red:
            UIColor.red.setFill()
            UIColor.red.setStroke()
        case .green:
            UIColor.green.setFill()
            UIColor.green.setStroke()
        case .purple:
            UIColor.purple.setFill()
            UIColor.purple.setStroke()
        }
    }
    
    private func drawSymbols(rect: CGRect) {
        let path: UIBezierPath
        switch symbol {
        case .squiggle:
            path = drawSquiggle(rect: rect)
        case .oval:
            path = drawOval(rect: rect)
        case .diamond:
            path = drawDiamond(rect: rect)
        }
        
        path.lineWidth = 2.5
        path.stroke()
        
        switch fill {
        case .stripes:
            stripeSymbol(symbol: path, rect: rect)
        case .solid:
            path.fill()
        default:
            break
        }
    }
    
    private func drawSquiggle(rect: CGRect) -> UIBezierPath {
        let upperSquiggle = UIBezierPath()
        let sqdx = rect.width * 0.1
        let sqdy = rect.height * 0.2
        upperSquiggle.move(to: CGPoint(x: rect.minX,
                                       y: rect.midY))
        upperSquiggle.addCurve(to:
            CGPoint(x: rect.minX + rect.width * 1/2,
                    y: rect.minY + rect.height / 8),
                               controlPoint1: CGPoint(x: rect.minX,
                                                      y: rect.minY),
                               controlPoint2: CGPoint(x: rect.minX + rect.width * 1/2 - sqdx,
                                                      y: rect.minY + rect.height / 8 - sqdy))
        upperSquiggle.addCurve(to:
            CGPoint(x: rect.minX + rect.width * 4/5,
                    y: rect.minY + rect.height / 8),
                               controlPoint1: CGPoint(x: rect.minX + rect.width * 1/2 + sqdx,
                                                      y: rect.minY + rect.height / 8 + sqdy),
                               controlPoint2: CGPoint(x: rect.minX + rect.width * 4/5 - sqdx,
                                                      y: rect.minY + rect.height / 8 + sqdy))
        upperSquiggle.addCurve(to:
            CGPoint(x: rect.minX + rect.width,
                    y: rect.minY + rect.height / 2),
                               controlPoint1: CGPoint(x: rect.minX + rect.width * 4/5 + sqdx,
                                                      y: rect.minY + rect.height / 8 - sqdy ),
                               controlPoint2: CGPoint(x: rect.minX + rect.width,
                                                      y: rect.minY))
        let lowerSquiggle = UIBezierPath(cgPath: upperSquiggle.cgPath)
        lowerSquiggle.apply(CGAffineTransform.identity.rotated(by: CGFloat.pi))
        lowerSquiggle.apply(CGAffineTransform.identity.translatedBy(x: bounds.width, y: bounds.height))
        upperSquiggle.move(to: CGPoint(x: rect.minX, y: rect.midY))
        upperSquiggle.append(lowerSquiggle)
        return upperSquiggle
    }
    
    private func drawOval(rect: CGRect) -> UIBezierPath {
        let oval = UIBezierPath()
        let radius = rect.height / 2
        oval.addArc(withCenter: CGPoint(x: rect.minX + radius,
                                        y: rect.minY + radius),
                    radius: radius,
                    startAngle: CGFloat.pi / 2,
                    endAngle: CGFloat.pi * 3/2,
                    clockwise: true)
        oval.addLine(to: CGPoint(x: rect.maxX - radius,
                                 y: rect.minY))
        oval.addArc(withCenter: CGPoint(x: rect.maxX - radius,
                                        y: rect.maxY - radius),
                    radius: radius,
                    startAngle: CGFloat.pi * 3/2,
                    endAngle: CGFloat.pi / 2,
                    clockwise: true)
        oval.close()
        return oval
    }
    
    private func drawDiamond(rect: CGRect) -> UIBezierPath {
        let diamond = UIBezierPath()
        diamond.move(to: CGPoint(x: rect.minX, y: rect.midY))
        diamond.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        diamond.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        diamond.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        diamond.close()
        return diamond
    }
    
    private func stripeSymbol(symbol: UIBezierPath, rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        symbol.addClip()
        makeStripe(rect: rect)
        context?.restoreGState()
    }
    
    private func makeStripe(rect: CGRect) {
        let stripe = UIBezierPath()
        stripe.lineWidth = 1.0
        stripe.move(to: CGPoint(x: rect.minX, y: bounds.minY))
        stripe.addLine(to: CGPoint(x: rect.minX, y: bounds.maxY))
        let stripeCount = Int(faceFrame.width / interStripeSpace)
        for _ in 1...stripeCount {
            stripe.apply(CGAffineTransform(translationX: interStripeSpace, y: 0))
            stripe.stroke()
        }
    }
    
    private func configureState() {
        backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        isOpaque = false
        contentMode = .redraw
        
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1).cgColor
        if isMatched {
            layer.borderColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        }
        if isSelected {
            layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureState()
    }
    
    func startAnimation(from startPoint: CGPoint, delay: TimeInterval) {
        let currentCenter = center
        let currentBounds = bounds
        
        center =  startPoint
        alpha = 0
        bounds = CGRect(x: 0.0, y: 0.0, width: 0.4 * bounds.width, height: 0.4 * bounds.height)
        isFaceUp = false
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = currentCenter
                self.bounds = currentBounds
                self.alpha = 1
            },
            completion: { position in
                UIView.transition(with: self,
                                  duration: 0.3,
                                  options: [.transitionFlipFromLeft],
                                  animations: {
                                      self.isFaceUp = true
                                  }
                )
            }
        )
    }
    
    var addDiscardPile : (() -> Void)?
    
    func animateFly(to discardPileCenter: CGPoint, delay: TimeInterval) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = discardPileCenter
            },
            completion: { position in
                UIView.transition(
                    with: self,
                    duration: 0.75,
                    options: [.transitionFlipFromLeft],
                    animations: {
                        self.isFaceUp = false
                        //self.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi / 2.0)
                        self.bounds = CGRect(x: 0.0, y: 0.0,
                                             width: 0.7 * self.bounds.width,
                                             height: 0.7 * self.bounds.height)
                    },
                    completion: { finished in
                        self.addDiscardPile?()
                        self.alpha = 0
                    }
                )
            }
        )
    }
}

extension SetCardView {
    private struct SizeRatio {
        static let maxFaceSizeToBoundsSize: CGFloat = 0.75
        static let pipHeightToFaceHeight: CGFloat = 0.25
        static let cornerRadiusToBoundsHeight: CGFloat = 0.06
    }
    
    private var cornerRadius: CGFloat {
        return bounds.size.height * SizeRatio.cornerRadiusToBoundsHeight
    }
    
    private struct AspectRatio {
        static let faceFrame: CGFloat = 0.60
    }
    
    private var maxFaceFrame: CGRect {
        return bounds.zoomed(by: SizeRatio.maxFaceSizeToBoundsSize)
    }
    
    private var faceFrame: CGRect {
        let faceWidth = maxFaceFrame.height * AspectRatio.faceFrame
        return maxFaceFrame.insetBy(dx: (maxFaceFrame.width - faceWidth) / 2, dy: 0)
    }
    
    private var pipHeight: CGFloat {
        return faceFrame.height * SizeRatio.pipHeightToFaceHeight
    }
    
    private var interPipHeight: CGFloat {
        return (faceFrame.height - (3 * pipHeight)) / 2
    }
}

extension SetCardView: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let newCardView = SetCardView()
        newCardView.color = color
        newCardView.count = count
        newCardView.fill = fill
        newCardView.symbol = symbol
        newCardView.isSelected = isSelected
        newCardView.bounds = bounds
        newCardView.frame = frame
        newCardView.alpha = 1
        return newCardView
    }
}

