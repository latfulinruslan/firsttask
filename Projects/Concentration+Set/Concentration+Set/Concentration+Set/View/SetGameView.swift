//
//  SetGameView.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class SetGameView: UIView {
    
    var cards = [SetCardView]()
    
    private var cardsInGrid: Grid?
    var gridRows: Int { return cardsInGrid?.dimensions.rowCount ?? 0 }
    
    func removeCards(_ cardViews: [SetCardView]) {
        for card in cardViews {
            cards.remove(at: cards.firstIndex(of: card)!)
            card.removeFromSuperview()
        }
        layoutIfNeeded()
    }
    
    func addCards(_ cardViews: [SetCardView]) {
        cards += cardViews
        for card in cardViews {
            addSubview(card)
        }
        layoutIfNeeded()
    }
    
    func newGame() {
        for card in cards {
            card.removeFromSuperview()
        }
        cards = []
        layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cardsInGrid = Grid(layout: Grid.Layout.aspectRatio(Constants.cellAspectRatio), frame: bounds)
        cardsInGrid?.cellCount = cards.count
        layoutCards()
    }
    
    private func layoutCards() {
        guard let gridCards = cardsInGrid else { return }
            for row in 0..<gridCards.dimensions.rowCount {
                for column in 0..<gridCards.dimensions.columnCount where ((row * gridCards.dimensions.columnCount + column) < cards.count) {
                    UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3,
                                                                   delay: TimeInterval(row) * 0.1,
                                                                   options: [],
                                                                   animations: {
                                                                    self.cards[row * gridCards.dimensions.columnCount + column].frame = gridCards[row, column]!.insetBy(dx: Constants.spaceDx, dy: Constants.spaceDy)
                    })
            }
        }
    }
}

