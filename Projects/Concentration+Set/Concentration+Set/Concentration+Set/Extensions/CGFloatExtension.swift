//
//  CGFloatExtension.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/19/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

extension CGFloat {
    var random: CGFloat {
        return self * (CGFloat(arc4random_uniform(UInt32.max))/CGFloat(UInt32.max))
    }
}
