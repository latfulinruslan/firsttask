//
//  IntExtension.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

extension Int {
    func random() -> Int {
        return Int.random(in: 0..<abs(self))
    }
}
