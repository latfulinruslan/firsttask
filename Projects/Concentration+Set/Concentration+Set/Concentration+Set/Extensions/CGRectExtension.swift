//
//  CGRectExtension.swift
//  Concentration+Set
//
//  Created by Ruslan Latfulin on 6/18/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

extension CGRect {
    func zoomed(by scale: CGFloat) -> CGRect {
        let newWidth = width * scale
        let newHeight = height * scale
        return insetBy(dx: (width - newWidth) / 2, dy: (height - newHeight) / 2)
    }
}
