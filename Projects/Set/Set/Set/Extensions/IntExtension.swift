//
//  IntExtension.swift
//  Set
//
//  Created by Ruslan Latfulin on 4/30/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

extension Int {
    func random() -> Int {
        return Int.random(in: 0..<abs(self))
    }
}
