//
//  Constants.swift
//  Set
//
//  Created by Ruslan Latfulin on 5/2/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

struct Constants {
    static let addMoreCards = 3
    static let maxCountCardsInGame = 81
    static let starterKit = 12
    static let cellAspectRatio: CGFloat = 0.7
    static let spaceDx: CGFloat = 2.0
    static let spaceDy: CGFloat = 2.0
}
