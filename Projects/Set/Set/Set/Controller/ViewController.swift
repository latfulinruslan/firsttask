//
//  ViewController.swift
//  Set
//
//  Created by Ruslan Latfulin on 4/29/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var game = SetGame()

    @IBOutlet weak var gameView: GameView! {
        didSet {
            let swipe = UISwipeGestureRecognizer(target: self, action: #selector(addThreeCards))
            swipe.direction = .down
            gameView.addGestureRecognizer(swipe)

            let rotation = UIRotationGestureRecognizer(target: self, action: #selector(reshuffle(sender:)))
            gameView.addGestureRecognizer(rotation)
        }
    }
    @IBOutlet private weak var addThreeCardsButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        newGame()
    }

    @IBAction private func pushAddThreeCardAction(_ sender: Any) {
        addThreeCards()
    }

    @objc private func addThreeCards() {
        guard game.fullDeck.count > 0 else { return }
        game.addCards(numberOfCards: Constants.addMoreCards)

        updateViewFromModel()
        if game.actualDeck.count == Constants.maxCountCardsInGame {
            addThreeCardsButton.isEnabled = false
        }
    }

    @objc private func reshuffle(sender: UIRotationGestureRecognizer) {
        guard sender.state == .ended else { return }
        game.reshuffle()
        updateViewFromModel()
    }

    @IBAction private func pushNewGameAction(_ sender: Any) {
        newGame()
    }

    private func newGame() {
        addThreeCardsButton.isEnabled = true
        game = SetGame()
        updateViewFromModel()
    }

    @IBAction private func pushFindSetAction(_ sender: Any) {
        guard game.actualDeck.count > 0 else { return }
        guard let set = findSet() else { return }
        for card in set {
            game.chooseCard(card: card)
        }
        updateViewFromModel()
    }

    private func findSet() -> [Card]? {
        var checkForSet = [Card]()

        for firstCardIndex in 0..<game.actualDeck.count {
            for secondCardIndex in (firstCardIndex + 1)..<game.actualDeck.count {
                for thirdCardIndex in (secondCardIndex + 1)..<game.actualDeck.count {
                    checkForSet.append(game.actualDeck[firstCardIndex])
                    checkForSet.append(game.actualDeck[secondCardIndex])
                    checkForSet.append(game.actualDeck[thirdCardIndex])
                    if game.isSet(cards: checkForSet) {
                        return checkForSet
                    } else {
                        checkForSet.removeAll()
                    }
                }
            }
        }
        return nil
    }

    private func updateViewFromModel() {
        if gameView.cards.count - game.actualDeck.count > 0 {
            let cards = gameView.cards[..<game.actualDeck.count]
            gameView.cards = Array(cards)
        }

        let cardsCount = gameView.cards.count
        for index in game.actualDeck.indices {
            let card = game.actualDeck[index]
            if index > (cardsCount - 1) {
                let newCard = CardView()
                updateCardView(newCard, for: card)
                addTapGestureRecognizer(for: newCard)
                gameView.cards.append(newCard)
            } else {
                let newCard = gameView.cards[index]
                updateCardView(newCard, for: card)
            }
        }
    }

    private func updateCardView(_ cardView: CardView, for card: Card) {
        cardView.symbol = card.symbol
        cardView.color = card.color
        cardView.count = card.number
        cardView.fill = card.filling
        cardView.isSelected = game.selectedDeck.contains(card)
    }

    private func addTapGestureRecognizer(for cardView: CardView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapCard(recognizer:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        cardView.addGestureRecognizer(tap)
    }

    @objc private func tapCard(recognizer: UITapGestureRecognizer) {
        guard recognizer.state == .ended else { return }
        guard let cardView = recognizer.view as? CardView else { return }
        let card = game.actualDeck[gameView.cards.firstIndex(of: cardView)!]
        game.chooseCard(card: card)
        updateViewFromModel()
    }
}
