//
//  Set.swift
//  Set
//
//  Created by Ruslan Latfulin on 4/29/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

class SetGame {

    private (set) var fullDeck = [Card]()
    private (set) var actualDeck = [Card]()
    private (set) var selectedDeck = [Card]()

    init() {
        fullDeck.removeAll()
        actualDeck.removeAll()
        selectedDeck.removeAll()

        for number in CardNumber.allCases {
            for symbol in CardSymbol.allCases {
                for color in CardColor.allCases {
                    for filling in CardFilling.allCases {
                        let card = Card(number: number, symbol: symbol, color: color, filling: filling)
                        fullDeck.append(card)
                    }
                }
            }
        }
        fullDeck.shuffle()
        addCards(numberOfCards: Constants.starterKit)
    }

    func addCards(numberOfCards: Int) {
        for _ in 0..<numberOfCards {
            let selectedCard = fullDeck.remove(at: fullDeck.count.random())
            actualDeck.append(selectedCard)
        }
    }

    func chooseCard(card: Card) {
        if selectedDeck.count == 3 && isSet(cards: selectedDeck) {
            selectedDeck.forEach {currCard in
                guard let selectedCardIndex = actualDeck.firstIndex(of: currCard) else { return }
                actualDeck.remove(at: selectedCardIndex)
                if fullDeck.count > 0 {
                    let selectedCard = fullDeck.remove(at: fullDeck.count.random())
                    actualDeck.insert(selectedCard, at: selectedCardIndex)
                }
            }
            selectedDeck.removeAll()
        } else if selectedDeck.count == 3 && !isSet(cards: selectedDeck) {
            selectedDeck.removeAll()
        }

        guard let cardToSelect = selectedDeck.firstIndex(of: card) else {
            selectedDeck.append(card)
            return
        }
        selectedDeck.remove(at: cardToSelect)
    }

    func isSet(cards: [Card]) -> Bool {
        guard cards.count == 3 else { return false }
        let sum = [
            cards.reduce(0, { $0 + $1.number.rawValue }),
            cards.reduce(0, { $0 + $1.color.rawValue }),
            cards.reduce(0, { $0 + $1.symbol.rawValue }),
            cards.reduce(0, { $0 + $1.filling.rawValue })
        ]
        return sum.reduce(true, { $0 && ($1 % 3 == 0) })
    }

    func cardIsSelected(card: Card) -> Bool {
        return selectedDeck.firstIndex(of: card) != nil
    }

    func reshuffle() {
        actualDeck.shuffle()
    }
}
