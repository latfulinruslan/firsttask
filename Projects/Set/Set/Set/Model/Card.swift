//
//  Card.swift
//  Set
//
//  Created by Ruslan Latfulin on 4/29/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import Foundation

struct Card: Hashable {
    let number: CardNumber
    let symbol: CardSymbol
    let color: CardColor
    let filling: CardFilling

    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

enum CardNumber: Int, CaseIterable {
    case one = 1, two, three
}

enum CardSymbol: Int, CaseIterable {
    case squiggle = 1, oval, diamond
}

enum CardColor: Int, CaseIterable {
    case red = 1, green, purple
}

enum CardFilling: Int, CaseIterable {
    case empty = 1, stripes, solid
}
