//
//  GameView.swift
//  Set
//
//  Created by Ruslan Latfulin on 6/7/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class GameView: UIView {

    var cards = [CardView]() {
        willSet { removeSubviews() }
        didSet {
            addSubviews()
            setNeedsLayout()
        }
    }

    private func removeSubviews() {
        for card in cards {
            card.removeFromSuperview()
        }
    }

    private func addSubviews() {
        for card in cards {
            addSubview(card)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        var grid = Grid(layout: Grid.Layout.aspectRatio(Constants.cellAspectRatio), frame: bounds)
        grid.cellCount = cards.count
        for row in 0..<grid.dimensions.rowCount {
            for column in 0..<grid.dimensions.columnCount where ((row * grid.dimensions.columnCount + column) < cards.count) {
                cards[row * grid.dimensions.columnCount + column].frame = grid[row, column]!.insetBy(dx: Constants.spaceDx, dy: Constants.spaceDy)
            }
        }
    }
}
