//
//  LabelExtension.swift
//  UserInformation
//
//  Created by Ruslan Latfulin on 4/23/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

extension UILabel {
    func thonburi() {
        self.font = UIFont(name: "Thonburi", size: 26)!
    }

    func centerTextAligment() {
        self.textAlignment = .center
    }
}
