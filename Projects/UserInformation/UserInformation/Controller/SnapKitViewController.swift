//
//  SnapKitViewController.swift
//  UserInformation
//
//  Created by Ruslan Latfulin on 4/17/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit
import SnapKit

class SnapKitViewController: UIViewController {

    lazy var profileImageView = CustomImageView()
    lazy var firstNameLabel = UILabel()
    lazy var secondNameLabel = UILabel()
    let fullNameStackView = UIStackView()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.view.addSubview(profileImageView)
        self.view.addSubview(fullNameStackView)

        initializeComponents()
        setStackViewProperties()
        setLabelProperties()
        addComponentsConstrains()
    }

    func initializeComponents() {
        profileImageView.image = UIImage(named: "profilePhoto")
        firstNameLabel.text = "Руслан"
        secondNameLabel.text = "Латфулин"
    }

    func setLabelProperties() {
        firstNameLabel.thonburi()
        secondNameLabel.thonburi()

        firstNameLabel.centerTextAligment()
        secondNameLabel.centerTextAligment()
    }

    func setStackViewProperties() {
        fullNameStackView.addArrangedSubview(firstNameLabel)
        fullNameStackView.addArrangedSubview(secondNameLabel)
        fullNameStackView.spacing = 5
        fullNameStackView.axis = .vertical
    }

    func addComponentsConstrains() {
        profileImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.center.x)
            make.bottom.equalTo(fullNameStackView.snp.top).offset(-30)
            make.left.equalTo(50)
            make.height.equalTo(profileImageView.snp.width)
        }

        fullNameStackView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.center.x)
            make.centerY.equalTo(self.view.center.y + 80)
        }
    }
}
