//
//  StartViewController.swift
//  UserInformation
//
//  Created by Ruslan Latfulin on 4/17/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBAction func pushSnapKitAction(_ sender: Any) {
        let snapKitController = SnapKitViewController()
        navigationController?.pushViewController(snapKitController, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
