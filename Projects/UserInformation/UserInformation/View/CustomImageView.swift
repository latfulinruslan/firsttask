//
//  CustomImageView.swift
//  UserInformation
//
//  Created by Ruslan Latfulin on 4/10/19.
//  Copyright © 2019 Ruslan Latfulin. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        customizeView()
    }

    func customizeView() {
        self.contentMode = .scaleAspectFill
        self.layer.cornerRadius = self.bounds.size.width / 2
        self.clipsToBounds = true
    }
}
